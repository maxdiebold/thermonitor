"""
This module puts the terminal into cbreak mode, so that characters
can be read one at a time, and restores the old settings on exit.
"""
from __future__ import annotations, print_function

import sys
import select
import tty
import termios
#import atexit

class Reader:
    def __init__(self):
        self._old_settings = termios.tcgetattr(sys.stdin)

    @staticmethod
    def key_pressed():
        return select.select([sys.stdin], [], [], 0) == ([sys.stdin], [], [])

    @staticmethod
    def read_key():
        """Waits for and reads one character from stdin"""
        return sys.stdin.read(1)

    def restore_settings(self):
        """Restores original tty settings"""
        termios.tcsetattr(sys.stdin, termios.TCSADRAIN, self._old_settings)

    @staticmethod
    def start():
        try:
            #atexit.register(restore_settings)
            tty.setcbreak(sys.stdin.fileno())
        except:
            print("Can't deal with your keyboard!")


if __name__ == "__main__":
    reader = Reader()
    print("Press any key")
    while True:
        char = reader.read_key()
        if char == ' ':
            print(f"Space: {ord(char)}")
        else:
            print(f"{char}: {ord(char)}")
