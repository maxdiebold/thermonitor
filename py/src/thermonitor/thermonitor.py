"""
The main entry point to the Thermonitor application.
This displays a dashboard of sensor display panels with live updates.
The cursor is used to select a sensor, and key commands are used to
perform actions on it: add/move/remove/edit or view detailed timeline
and location data.
"""
from __future__ import annotations
import argparse
import os
import re
from threading import Event, Thread
import time
from typing import TYPE_CHECKING

import requests
from rich.align import Align
from rich.console import Console
from rich.layout import Layout
from rich.live import Live
from rich.padding import Padding

from config import HOSTNAME, Layouts
from context import Context
from figlettext import FigletText
from keylistener import KeyListener
from reader import Reader
from sensors import Sensors
from user import User

if TYPE_CHECKING:
    from argparse import Namespace

def main():
    """Runs program until stop event is raised via 'Ctrl-c'
    or one of the designated keys
    """
    stop_event = Event()
    reader = Reader()
    try:
        run(stop_event, reader)
    except KeyboardInterrupt:
        pass
    finally:
        stop_event.set()
        reader.restore_settings()

def run(stop_event: Event, reader: Reader):
    args = parse_args()

    layout = build_layout()

    while True:
        user = User()
        user.log_in()
        stop_event.clear()

        reader.start()
        context = configure_context(args, layout, user, stop_event, reader)

        populate_layout(context)

        start_tasks(context)

        # start the live display
        with Live(layout, transient=True) as live:
            lock = context.sensors.get_lock()
            while context.user.authorized is True:
                context.listener.handle_char()
                time.sleep(.01)
                with lock:
                    live.refresh()
        stop_event.set()
        reader.restore_settings()

def build_detail_layout(layout: Layout):
    layout["detail"].split_row(Layout(name=Layouts.INFO.value, ratio=1),
                               Layout(name=Layouts.TIMELINE.value, ratio=1))
    layout["info"].split_column(Layout(name=Layouts.SENSOR_INFO.value, ratio=1),
                                Layout(name=Layouts.LOCATION_INFO.value, ratio=1))
    build_timeline_layout(layout)

def build_header_layout(layout: Layout):
    layout["header"].split_row(Layout(name=Layouts.TITLE.value, ratio=3),
                               Layout(name=Layouts.TOOLTIP.value, ratio=2))
    layout["tooltip"].split_row(Layout(name=Layouts.TOOLTIP_SPINNER.value, ratio=1),
                                Layout(name=Layouts.TOOLTIP_CONTENT.value, ratio=1))
    layout["tooltip spinner"].visible = False

def build_layout() -> Layout:
    """Builds the layout skeleton to be rendered by rich"""
    layout = Layout()

    layout.split(Layout(name=Layouts.HEADER.value, ratio=1, minimum_size=4),
                 Layout(name=Layouts.MAIN.value, ratio=6))
    build_header_layout(layout)
    build_main_layout(layout)
    build_detail_layout(layout)

    return layout

def build_main_layout(layout: Layout):
    layout["main"].split_column(Layout(name=Layouts.DASH.value, ratio=1),
                                Layout(name=Layouts.DETAIL.value, ratio=1),
                                Layout(name=Layouts.HELP.value, ratio=1),
                                Layout(name=Layouts.SPINNER.value, ratio=1))
    layout["detail"].visible = False
    layout["help"].visible = False
    layout["spinner"].visible = False

def build_timeline_layout(layout: Layout):
    layout["timeline"].split_column(Layout(name=Layouts.TEMPERATURE.value, ratio=1),
                                    Layout(name=Layouts.HUMIDITY.value, ratio=1))
    layout["temperature"].split_row(Layout(name=Layouts.TEMPERATURE_TIMELINE.value, ratio=1),
                                    Layout(name=Layouts.TEMPERATURE_SPINNER.value, ratio=1))
    layout["humidity"].split_row(Layout(name=Layouts.HUMIDITY_TIMELINE.value, ratio=1),
                                 Layout(name=Layouts.HUMIDITY_SPINNER.value, ratio=1))
    layout["temperature spinner"].visible = False
    layout["humidity spinner"].visible = False

def configure_context(args: Namespace, layout: Layout, user: User,
                      stop_event: Event, reader: Reader) -> Context:
    """Creates the application context, manages state"""
    file = (os.path.join(os.path.dirname(__file__), "demo.conf")
            if user.username == "demo" else args.file)
    context = Context(user, file)
    context.layout = layout
    sensors = Sensors(context, stop_event)
    context.sensors = sensors
    listener = KeyListener(context.on_key,
                           stop_event,
                           reader,
                           sensors.get_lock())
    context.listener = listener
    context.change_state("normal")
    context.load_config()
    return context

def parse_args() -> Namespace:
    """Gets the command line arguments"""
    parser = argparse.ArgumentParser()
    default_state = os.path.join(os.path.dirname(__file__), '.thermonitor.conf')
    parser.add_argument("--file", '-f', help="file location for program state", \
                        default=default_state)
    return parser.parse_args()

def populate_layout(context: Context):
    """Fills the layout skeleton with objects to render"""
    layout = context.layout
    sensors = context.sensors
    layout["title"].update(
        Padding(Align.center(FigletText("Thermonitor"), vertical="middle"), (0, 1)))
    layout["dash"].update(Align.center(sensors))

def start_tasks(context: Context):
    """Starts the sensor update thread and the key listener thread"""
    # start task to update sensor data
    sensor_task = Thread(target=context.sensors.run, daemon=True)
    sensor_task.start()

    # start key listener
    listener_task = Thread(target=context.listener.listen, daemon=True)
    listener_task.start()


if __name__ == "__main__":
    main()
