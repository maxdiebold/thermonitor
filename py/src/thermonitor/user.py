from __future__ import annotations
import re

import requests
from rich.console import Console
from rich.padding import Padding
from rich.prompt import Prompt
from rich.table import Table

from config import Colors, HOSTNAME

class User:
    PSTYLE = f"[bold {Colors.PURPLE.value}]"
    PADDING = (1, 4)

    def __init__(self):
        self._authorized = False
        self._console = Console()
        self._id_token = ''
        self._refresh_token = ''
        self._username = ''

    def _authorize(self, username: str, password: str) -> bool:
        payload = {
            "username": username,
            "password": password,
        }
        endpoint = (f"https://{HOSTNAME}/sessions")
        response = requests.post(endpoint, json=payload)
        if response.status_code == 200:
            result = response.json()
            self._id_token = result["id_token"]
            self._refresh_token = result["refresh_token"]
            self._username = username
            self._authorized = True
            self._console.clear()
        return self._authorized

    def _authenticate(self):
        heading = "[bold green]Please log in"
        while self._authorized is False:
            self._console.print(Padding(heading, self.PADDING))
            username = Prompt.ask(f"{self.PSTYLE}Username")
            password = Prompt.ask(f"{self.PSTYLE}Password", password=True)
            with self._console.status("Checking..."):
                authorized = self._authorize(username, password)
                if not authorized:
                    heading = "[bold red]Login failed"

    def _create_user(self, username: str, password: str, email: str) -> bool:
        with self._console.status("[bold green]Creating user..."):
            endpoint = f"https://{HOSTNAME}/users"
            payload = {
                "username": username,
                "email": email,
                "password": password,
            }
            response = requests.post(endpoint, json=payload)
            if response.status_code == 200:
                return self._authorize(username, password)
            return False

    def log_in(self):
        heading = "[bold green]Thermonitor"
        while self._authorized is False:
            self._console.print(self._log_in_table(heading))
            choice = Prompt.ask("Select an option", choices=["1", "2", "3"],
                                default="3", show_choices=False)
            #self._console.clear()
            try:
                self._handle_option(choice)
            except ValueError:
                heading = "[bold red]Invalid selection"

    def _log_in_table(self, heading):
        table = Table(box=None, title=heading, show_header=True, show_edge=False)
        table.add_column(style="bold cyan")
        table.add_column()
        table.add_row("1.", "Sign in")
        table.add_row("2.", "Register")
        table.add_row("3.", "Demo")
        return Padding(table, self.PADDING)

    @property
    def authorized(self) -> bool:
        return self._authorized

    @authorized.setter
    def authorized(self, value: bool):
        self._authorized = value

    def _check_username(self, username: str):
        with self._console.status("[bold green]Checking availability..."):
            endpoint = f"https://{HOSTNAME}/users/{username}"
            response = requests.get(endpoint)
            if response.status_code == 200:
                #self._console.clear()
                return False
            return True

    @property
    def id_token(self) -> str:
        return self._id_token

    @id_token.setter
    def id_token(self, value: str):
        self._id_token = value

    def _handle_option(self, choice: str):
        option = int(choice)
        if option == 1:
            self._authenticate()
        elif option == 2:
            self._register_user()
        elif option == 3:
            self._start_demo()
        else:
            raise ValueError

    def refresh(self):
        payload = {
            "username": self._username,
            "refresh_token": self._refresh_token,
        }
        endpoint = (f"https://{HOSTNAME}/sessions")
        response = requests.post(endpoint, json=payload)
        if response.status_code == 200:
            result = response.json()
            self._id_token = result["id_token"]
        else:
            self._authorized = False

    def _register_user(self):
        heading = "[bold green]Register a new user"
        email_format = re.compile(r"[^@]+@[^@]+\.[^@]+")
        password_reg = r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d@$!#%*?&]{6,20}$"
        password_format = re.compile(password_reg)

        while self._authorized is False:
            self._console.print(Padding(heading, self.PADDING))
            username = Prompt.ask(f"{self.PSTYLE}Username")
            available = self._check_username(username)
            if not available:
                heading = "[bold red]Username is taken"
                continue
            email = Prompt.ask(f"{self.PSTYLE}Email")
            if not email_format.fullmatch(email):
                heading = "[bold red]Invalid email"
                continue
            pass_help = ("[bold green]Choose a password 10-20 characters long\n"
                         "[bold green]that includes at least one lowercase letter,\n"
                         "[bold green]one uppercase letter, and one digit")
            self._console.print(Padding(pass_help, self.PADDING))
            password = Prompt.ask(f"{self.PSTYLE}Password", password=True)
            if not re.search(password_format, password):
                heading = "[bold red]Invalid password"
                continue
            password_confirm = Prompt.ask(f"{self.PSTYLE}Confirm password", password=True)
            if password != password_confirm:
                heading = "[bold red]Passwords do not match"
                continue
            user_created = self._create_user(username, password, email)
            if not user_created:
                heading = "[bold red]Email is already in use"

    @property
    def refresh_token(self) -> str:
        return self._refresh_token

    @refresh_token.setter
    def refresh_token(self, value: str):
        self._refresh_token = value

    def _start_demo(self):
        self._authorize("demo", "Password12")

    @property
    def username(self) -> str:
        return self._username

    @username.setter
    def username(self, value: str):
        self._username = value
