import time

import json
import math
import random
import requests

ID = "eyJraWQiOiJCeWZhWnJsQ0ZXUnlyYXBxVUQ1Vzh6Y2RZUTdaMkRTYlNsSTNEazh4ak1RPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiI1YWM4YTU1NS04ZDhlLTRhOTItOTJjZC0yNGJlYjhiODY0YjUiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLnVzLWVhc3QtMi5hbWF6b25hd3MuY29tXC91cy1lYXN0LTJfanBta1FaOHFkIiwiY29nbml0bzp1c2VybmFtZSI6ImRlbW8iLCJvcmlnaW5fanRpIjoiMjdmYTBjYjgtZmI4OC00Y2IxLWE3MmEtYjE4ZjA5OTE0YzllIiwiYXVkIjoia2h1YnFidGI5ZzdqbmhudW5uaWh0MDIwbiIsImV2ZW50X2lkIjoiOTIyMTBiOTktMjYyMi00OTczLThmMDktMjBkMmI3OTg3NjgxIiwidG9rZW5fdXNlIjoiaWQiLCJhdXRoX3RpbWUiOjE2MzkxMjI5OTUsImV4cCI6MTYzOTIwOTM5NSwiaWF0IjoxNjM5MTIyOTk1LCJqdGkiOiI4MTYyNTc3Yi03MGVmLTQwNjEtYWIyNC05OGVkMmVjODMwZmUiLCJlbWFpbCI6Im1heGRib2xkQGdtYWlsLmNvbSJ9.k0MS7CeF4Eb-aHekGW5l14j_LPWRoYfU0Zr-yHItKv78zIhEZzKJ0BwgdQP9iE9uqM_QuEPMp7iiZsjxVT9eU3CszCBbtrK3aGi-JVj0RlErxdHuiJIcZB08sz71OPsk4oy30GLmuxJskPsy0f0utaViRmUtBnVQ1IS7yIWAgxMkzuEqQbZCjM7zMoXXeM_MyXlfVg0xKiK4Jy-A-8O-jWCm0x1kDpZZp0YWnJPZBpAEG5aoujsVyxYtfwrIoSixhMCVNcwTP6pOCeg7Q_cCKIoZSksMOJi313fOOe1RGp0mLYhy4XstpGTxG8hhVPX6wyjtL-gotW35RQMZ3mgAHw"

URL = "https://bko7deq544.execute-api.us-east-2.amazonaws.com/dev/sensors/demo"
headers = {'authorization-token': ID}
def main():
    while True:
        payload = {
            "EpochTime": math.floor(time.time()),
            "LocationId": "45203",
            "DeviceId": "test",
            "Temperature": random.randrange(-10, 121, 1),
            "Humidity": random.randrange(0, 101, 1),
        }
        body = json.dumps(payload)
        r = requests.post(URL, headers=headers, data=body)
        print(r.text)
        time.sleep(5)

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
