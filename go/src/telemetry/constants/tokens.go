package constants

// Tokens for access to the Telemetry Api Gateway API using request authorization
const (
	SensorsToken  = "allow"
	ScitizenToken = "39dfjlekjaoviIEJ33To"
	DogsToken     = "92DHHeosppfjlHW123Ed"
)
