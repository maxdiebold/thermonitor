package constants

const (
	// ClientID is for the thermonitor app in the thermonitor-users
	// Amazon Cognito user pool
	ClientID = "khubqbtb9g7jnhnunniht020n"
	// Region is the region of the AWS physical data center cluster
	Region = "us-east-2"
	// TableName is the name of the DynamoDB table accessed by the Telemetry Api Gateway
	TableName = "Telemetry"
	// UserPoolID is for the thermonitor-users Amazon Cognito user pool
	UserPoolID = "us-east-2_jpmkQZ8qd"
)
