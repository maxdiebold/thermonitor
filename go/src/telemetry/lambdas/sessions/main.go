package main

import (
	"context"
	"encoding/json"
	"log"
	"time"

	cognitosrp "github.com/alexrudd/cognito-srp/v4"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	cip "github.com/aws/aws-sdk-go-v2/service/cognitoidentityprovider"
	"github.com/aws/aws-sdk-go-v2/service/cognitoidentityprovider/types"

	"telemetry/constants"
	"telemetry/utils"
)

func authenticationSuccessResponse(authenticationResult types.AuthenticationResultType) (events.APIGatewayProxyResponse, error) {
	tokens := map[string]string{
		"id_token":      *authenticationResult.IdToken,
		"refresh_token": *authenticationResult.RefreshToken,
	}
	json, jsonErr := json.Marshal(tokens)
	if jsonErr != nil {
		log.Fatalf("Could not encode results: %v", jsonErr)
	}

	return events.APIGatewayProxyResponse{
		Body: string(json),
		// The lambda handler includes necessary CORS headers in the API Gateway response
		Headers: map[string]string{
			"Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization," +
				"X-Api-Key,X-Amz-Security-Token,authorization-token",
			"Access-Control-Allow-Origin":  "*",
			"Access-Control-Allow-Methods": "OPTIONS,POST,GET",
		},
		StatusCode: 200,
	}, nil
}

func decodePostData(request *events.APIGatewayProxyRequest) map[string]string {
	body := []byte(request.Body)
	var credentials map[string]string

	if err := json.Unmarshal(body, &credentials); err != nil {
		log.Fatalln("Could not decode data")
	}
	if _, usernameOk := credentials["username"]; !usernameOk {
		log.Fatalln("username is required")
	}
	return credentials
}

func notAuthorizedResponse() (events.APIGatewayProxyResponse, error) {
	return events.APIGatewayProxyResponse{
		Body: "User not authorized",
		Headers: map[string]string{
			"Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization," +
				"X-Api-Key,X-Amz-Security-Token,authorization-token",
			"Access-Control-Allow-Origin":  "*",
			"Access-Control-Allow-Methods": "OPTIONS,POST,GET",
		},
		StatusCode: 401,
	}, nil
}

// sessionsEndpointHandler is an AWS Lambda function
// that authenticates a user with Amazon Cognito.
func sessionsEndpointHandler(
	request events.APIGatewayProxyRequest,
) (events.APIGatewayProxyResponse, error) {

	// This handler only handles POST requests.
	if request.HTTPMethod == "POST" {
		// Initialize the Cognito Identity Provider service
		cfg, cfgErr := config.LoadDefaultConfig(
			context.TODO(),
			config.WithRegion(constants.Region),
			//config.WithCredentialsProvider(aws.AnonymousCredentials{}),
		)
		if cfgErr != nil {
			log.Fatalf("Failed to load configuration, %v", cfgErr)
		}
		svc := cip.NewFromConfig(cfg)

		credentials := decodePostData(&request)

		var password string
		var authParams map[string]string
		var authFlow types.AuthFlowType

		refreshToken, refreshTokenOk := credentials["refresh_token"]
		if refreshTokenOk {
			password = ""
		} else {
			if _, passwordOk := credentials["password"]; !passwordOk {
				log.Fatalln("password is required")
			}
			password = credentials["password"]
		}

		csrp, csrpErr := cognitosrp.NewCognitoSRP(
			credentials["username"],
			password,
			constants.UserPoolID,
			constants.ClientID,
			aws.String(constants.ClientSecret),
		)
		if csrpErr != nil {
			log.Fatalln("Could not generate SRP")
		}

		if refreshTokenOk {
			secretHash, secretHashErr := csrp.GetSecretHash(credentials["username"])
			if secretHashErr != nil {
				log.Fatalf("Error encoding secret hash: %v", secretHashErr)
			}
			authFlow = types.AuthFlowTypeRefreshTokenAuth
			authParams = map[string]string{
				"REFRESH_TOKEN": refreshToken,
				"SECRET_HASH":   secretHash,
			}
		} else {
			authParams = csrp.GetAuthParams()
			authFlow = types.AuthFlowTypeUserSrpAuth
		}

		var result types.AuthenticationResultType

		initiateOutput, initiateOutputErr := svc.InitiateAuth(context.TODO(), &cip.InitiateAuthInput{
			AuthFlow:       authFlow,
			ClientId:       aws.String(csrp.GetClientId()),
			AuthParameters: authParams,
		})
		if initiateOutputErr != nil {
			log.Fatalf("Authentication error: %v", initiateOutputErr)
			return notAuthorizedResponse()
		}
		if initiateOutput.ChallengeName == types.ChallengeNameTypePasswordVerifier {
			challengeResponses, _ := csrp.PasswordVerifierChallenge(
				initiateOutput.ChallengeParameters,
				time.Now(),
			)

			challengeOutput, challengeOutputErr := svc.RespondToAuthChallenge(
				context.Background(),
				&cip.RespondToAuthChallengeInput{
					ChallengeName:      types.ChallengeNameTypePasswordVerifier,
					ChallengeResponses: challengeResponses,
					ClientId:           aws.String(csrp.GetClientId()),
				},
			)
			if challengeOutputErr != nil {
				log.Fatalf("Password not verified: %v", challengeOutputErr)
			}

			result = *challengeOutput.AuthenticationResult
		} else {
			result = *initiateOutput.AuthenticationResult
		}

		return authenticationSuccessResponse(result)
	}
	return utils.MethodNotAllowedResponse()
}

func main() {
	lambda.Start(sessionsEndpointHandler)
}
