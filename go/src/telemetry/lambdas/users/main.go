package main

import (
	"context"
	"encoding/json"
	"errors"
	"log"
	"math/rand"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	cip "github.com/aws/aws-sdk-go-v2/service/cognitoidentityprovider"
	"github.com/aws/aws-sdk-go-v2/service/cognitoidentityprovider/types"

	"telemetry/constants"
	"telemetry/utils"
)

type result struct {
	Username   string
	Attributes []types.AttributeType
}

func decodePostData(request *events.APIGatewayProxyRequest) map[string]string {
	body := []byte(request.Body)
	var credentials map[string]string

	if err := json.Unmarshal(body, &credentials); err != nil {
		log.Fatalln("Could not decode data")
	}
	if _, usernameOk := credentials["username"]; !usernameOk {
		log.Fatalln("username is required")
	}
	if _, passwordOk := credentials["password"]; !passwordOk {
		log.Fatalln("password is required")
	}
	if _, emailOk := credentials["email"]; !emailOk {
		log.Fatalln("email is required")
	}
	return credentials
}

func generatePassword() string {
	rand.Seed(time.Now().UnixNano())
	digits := "0123456789"
	upper := "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	lower := "abcdefghijklmnopqrstuvwxyz"
	all := upper + lower + digits
	length := 10
	buf := make([]byte, length)
	buf[0] = digits[rand.Intn(len(digits))]
	buf[1] = upper[rand.Intn(len(upper))]
	buf[2] = lower[rand.Intn(len(lower))]
	for i := 2; i < length; i++ {
		buf[i] = all[rand.Intn(len(all))]
	}
	rand.Shuffle(len(buf), func(i, j int) {
		buf[i], buf[j] = buf[j], buf[i]
	})
	return string(buf)
}

func createUserSuccessResponse(userData result) (events.APIGatewayProxyResponse, error) {
	json, err := json.Marshal(userData)
	if err != nil {
		log.Fatalf("Could not encode results")
	}

	return events.APIGatewayProxyResponse{
		Body: string(json),
		// The lambda handler includes necessary CORS headers in the API Gateway response
		Headers: map[string]string{
			"Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization," +
				"X-Api-Key,X-Amz-Security-Token,authorization-token",
			"Access-Control-Allow-Origin":  "*",
			"Access-Control-Allow-Methods": "OPTIONS,POST,GET",
		},
		StatusCode: 200,
	}, nil
}

func getUserSuccessResponse(username string) (events.APIGatewayProxyResponse, error) {
	return events.APIGatewayProxyResponse{
		Body: username,
		Headers: map[string]string{
			"Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization," +
				"X-Api-Key,X-Amz-Security-Token,authorization-token",
			"Access-Control-Allow-Origin":  "*",
			"Access-Control-Allow-Methods": "OPTIONS,POST,GET",
		},
		StatusCode: 200,
	}, nil
}

func usernameExistsResponse() (events.APIGatewayProxyResponse, error) {
	return events.APIGatewayProxyResponse{
		Body: "Username already exists",
		Headers: map[string]string{
			"Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization," +
				"X-Api-Key,X-Amz-Security-Token,authorization-token",
			"Access-Control-Allow-Origin":  "*",
			"Access-Control-Allow-Methods": "OPTIONS,POST,GET",
		},
		StatusCode: 409,
	}, nil
}

func userNotFoundResponse() (events.APIGatewayProxyResponse, error) {
	return events.APIGatewayProxyResponse{
		Body: "User not found",
		Headers: map[string]string{
			"Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization," +
				"X-Api-Key,X-Amz-Security-Token,authorization-token",
			"Access-Control-Allow-Origin":  "*",
			"Access-Control-Allow-Methods": "OPTIONS,POST,GET",
		},
		StatusCode: 404,
	}, nil
}

// usersEndpointHandler is an AWS Lambda function that
// manages users in the thermonitor-users Amazon Cognito pool
func usersEndpointHandler(
	request events.APIGatewayProxyRequest,
) (events.APIGatewayProxyResponse, error) {

	// Initialize the Cognito Identity Provider service
	cfg, cfgErr := config.LoadDefaultConfig(
		context.TODO(),
		config.WithRegion(constants.Region),
		//config.WithCredentialsProvider(aws.AnonymousCredentials{}),
	)
	if cfgErr != nil {
		log.Fatalf("Failed to load configuration, %v", cfgErr)
	}
	svc := cip.NewFromConfig(cfg)

	// POST request creates a new user
	if request.HTTPMethod == "POST" {
		credentials := decodePostData(&request)
		temporary := generatePassword()

		createInput := &cip.AdminCreateUserInput{
			UserPoolId:        aws.String(constants.UserPoolID),
			Username:          aws.String(credentials["username"]),
			MessageAction:     types.MessageActionTypeSuppress,
			TemporaryPassword: aws.String(temporary),
			UserAttributes: []types.AttributeType{
				types.AttributeType{
					Name:  aws.String("email_verified"),
					Value: aws.String("True"),
				},
				types.AttributeType{
					Name:  aws.String("email"),
					Value: aws.String(credentials["email"]),
				},
			},
		}
		createOutput, createOutputErr := svc.AdminCreateUser(context.TODO(), createInput)
		if createOutputErr != nil {
			var usernameExists *types.UsernameExistsException
			if errors.As(createOutputErr, &usernameExists) {
				return usernameExistsResponse()
			}
			log.Fatalf("Error creating user: %v", createOutputErr)
		}

		passwordInput := &cip.AdminSetUserPasswordInput{
			Password:   aws.String(credentials["password"]),
			UserPoolId: aws.String(constants.UserPoolID),
			Username:   aws.String(credentials["username"]),
			Permanent:  true,
		}
		_, passwordOutputErr := svc.AdminSetUserPassword(context.TODO(), passwordInput)
		if passwordOutputErr != nil {
			log.Fatalf("Could not set user password: %v", passwordOutputErr)
		}

		outputUsername := createOutput.User.Username
		outputAttributes := createOutput.User.Attributes
		userData := result{
			Username:   *outputUsername,
			Attributes: outputAttributes,
		}
		return createUserSuccessResponse(userData)
	} else if request.HTTPMethod == "GET" {
		username := request.PathParameters["user"]

		userInput := &cip.AdminGetUserInput{
			UserPoolId: aws.String(constants.UserPoolID),
			Username:   aws.String(username),
		}
		userOutput, userOutputErr := svc.AdminGetUser(context.TODO(), userInput)
		if userOutputErr != nil {
			var userNotFound *types.UserNotFoundException
			if errors.As(userOutputErr, &userNotFound) {
				return userNotFoundResponse()
			}
			log.Fatalf("Error getting user: %v", userOutputErr)
		}
		outputUsername := userOutput.Username
		return getUserSuccessResponse(*outputUsername)
	}
	return utils.MethodNotAllowedResponse()
}

func main() {
	lambda.Start(usersEndpointHandler)
}
