Contains the AWS DynamoDB telemetry service project files

- AWS Lambda handler functions: [`src/telemetry/lambdas`](https://gitlab.com/maxdiebold/thermonitor/-/tree/master/go/src/telemetry/lambdas)
- utility functions: [`src/telemetry/utils`](https://gitlab.com/maxdiebold/thermonitor/-/tree/master/go/src/telemetry/utils)
