//! # Sensor
//! 
//! A simple program to access an I2C temperature/humidity sensor
//! and push the data to an AWS DynamoDB table.

use {
    aht20::Aht20,
    chrono::Utc,
    embedded_hal::blocking::delay::DelayMs,
    linux_embedded_hal as hal,
    reqwest::blocking::Client,
    serde::ser::{Serializer, SerializeStruct},
    serde::{Deserialize, Serialize},
    std::{env, fs, io, process},
    std::collections::HashMap,
    toml,
};

#[derive(Deserialize)]
struct Config {
    location: String,
    device_id: String,
    host: String,
    project_id: String,
}

#[derive(Deserialize, Serialize)]
struct Credentials {
    username: String,
    password: String,
}

#[derive(Deserialize)]
struct Tokens {
    id_token: String,
    refresh_token: String,
}

struct PostData {
    location: String,
    device: String,
    timestamp: i64,
    temperature: f32,
    humidity: f32,
}

impl Serialize for PostData {
    /// Defines how a PostData object is serialized for transmission
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut rgb = serializer.serialize_struct("PostData", 5)?;
        rgb.serialize_field("LocationId", &self.location)?;
        rgb.serialize_field("DeviceId", &self.device)?;
        rgb.serialize_field("EpochTime", &self.timestamp)?;
        rgb.serialize_field("Temperature", &self.temperature)?;
        rgb.serialize_field("Humidity", &self.humidity)?;
        rgb.end()
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("usage: {} /dev/i2c-N", args[0]);
        process::exit(1);
    }

    // Read config file
    let config_file = "config.toml";
    let config_text = fs::read_to_string(config_file)
        .expect("Error while reading configuration file");
    let config: Config = toml::from_str(&config_text).unwrap();

    // Get the user's credentials and authenticate with the telemetry API
    let credentials = get_credentials().unwrap();
    let auth_endpoint = format!("https://{}/sessions", config.host);
    let client = Client::new();
    let mut tokens = authenticate(&credentials, &auth_endpoint, &client);

    // Initialize the temperature/humidity sensor
    let i2c = hal::I2cdev::new(&args[1]).unwrap();
    let mut dev = Aht20::new(i2c, hal::Delay).unwrap();

    loop {
        let epoch_time: i64 = Utc::now().timestamp();

        let (h, t) = dev.read().unwrap();
        println!(
            "relative humidity={0}%; temperature={1}C",
            h.rh(),
            t.celsius()
        );

        let data_endpoint = format!("https://{}/{}/{}",
            config.host, config.project_id, credentials.username);

        let post_data = PostData {
            location: config.location.clone(),
            device: config.device_id.clone(),
            timestamp: epoch_time.into(),
            temperature: t.celsius().into(),
            humidity: h.rh().into(),
        };

        let res = client.post(&data_endpoint)
            .json(&post_data)
            .header("authorization-token", tokens.id_token.clone())
            .send()
            .unwrap();

        if res.status().is_success() {
            println!("success!");
            hal::Delay.delay_ms(60000u16);
        } else {
            refresh_tokens(&mut tokens, &credentials, &client, &auth_endpoint);
        }

    }
}

fn authenticate(credentials: &Credentials, endpoint: &String, client: &Client) -> Tokens {
    loop {
        let res = client.post(endpoint)
            .json(credentials)
            .send()
            .unwrap();

        if res.status().is_success() {
            let tokens = res.json().unwrap();
            break tokens
        } else {
            println!("Failed to authenticate");
            hal::Delay.delay_ms(5000u16);
        }
    }
}

fn get_credentials() -> Result<Credentials, io::Error> {
    let mut username = String::new();
    let mut password = String::new();
    let stdin = io::stdin();
    println!("Customer ID: ");
    stdin.read_line(&mut username)?;
    username = username.trim().to_string();
    println!("Password: ");
    stdin.read_line(&mut password)?;
    password = password.trim().to_string();
    print!("{esc}c", esc = 27 as char);
    let credentials =  Credentials {
        username: username,
        password: password,
    };
    Ok(credentials)
}

fn refresh_tokens(tokens: &mut Tokens, credentials: &Credentials,
        client: &Client, endpoint: &String) {
    let mut payload = HashMap::new();
    payload.insert("username", credentials.username.clone());
    payload.insert("refresh_token", tokens.refresh_token.clone());

    let response = client.post(endpoint)
        .json(&payload)
        .send()
        .unwrap();

    if response.status().is_success() {
        let result: Tokens = response.json().unwrap();
        tokens.id_token = result.id_token.clone();
    } else {
        let new_tokens = authenticate(credentials, endpoint, client);
        tokens.id_token = new_tokens.id_token.clone();
        tokens.refresh_token = new_tokens.refresh_token.clone();
    }
}
