# Thermonitor
Thermonitor is a TUI dashboard for IoT temperature/humidity sensors  

![Dashboard demo](demo.gif)

## Table of Contents
- [Dashboard client: `py/src/thermonitor`](https://gitlab.com/maxdiebold/thermonitor/-/tree/master/py/src/thermonitor)
- [Telemetry service: `go/src/telemetry`](https://gitlab.com/maxdiebold/thermonitor/-/tree/master/go/src/telemetry)
- [Weather service: `js/weather`](https://gitlab.com/maxdiebold/thermonitor/-/tree/master/js/weather)
- [Sensor client: `rs/sensor`](https://gitlab.com/maxdiebold/thermonitor/-/tree/master/rs/sensor/src)

## Installation
### From source
`conda env create --file environment.yaml`  
`conda activate thermonitor`  
`python3 -m pip install .`  

The binary application is located in the 'bin' directory of the thermonitor environment  

This will be a sibling of the 'lib' directory shown in the output of the following:  
`python3 -m pip show thermonitor`  

***OPTIONAL:*** start weather service  
`node js/weather/app.js`

From 'bin' directory found above:  
`./thermonitor`  

### Docker
`docker-compose run thermonitor`

***OR***  

`docker build --tag thermonitor .`  
`docker run -it --mount 'type=volume,src=myvol,dst=/app' -v /etc/localtime:/etc/localtime thermonitor`
