#!/bin/bash

set -m

node js/app.js &>/dev/null &

python3 py/thermonitor.py
